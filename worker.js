const child_process = require('child_process');
const tempfile = require('tempfile');
const fs = require('fs');
const uuid = require('uuid/v4');
const url = require('url');
const config = require('./config');

const redis = require('redis').createClient(config.redis);

redis.on('connect', function() {
    console.log('Redis client connected');
});

redis.on("error", function (err) {
    console.log("Error " + err);
});

const submitFlag = async (flag, teamID, adID) => {
    let _submitFlag = (flag) => {
        return new Promise((resolve, reject) => {
            let parsedUrl = url.parse(config.apiSubmitFlag);
            let client = parsedUrl.protocol === 'https' ? require('https') : require('http');
            let data = JSON.stringify({'teamID': teamID, 'adID': adID, 'flag': flag});
            let options = {
                hostname: parsedUrl.hostname,
                port: parsedUrl.port || 80,
                path: parsedUrl.path,
                method: 'POST',
                headers: {
                'Content-Type': 'application/json',
                'Content-Length': data.length
                }
            };
            let req = client.request(options, (res) => {
                let ret = '';
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    ret += chunk.toString();
                });
                res.on('end', (chunk) => {
                    resolve(ret);
                });
            });
            req.on('error', (e) => resolve('error'));
            req.on('data', (ret) => resolve(ret));
            req.write(data);
            req.end();
        });
    }
    const ret = await _submitFlag(flag);
    return ret;
};

const execute = () => {
    redis.brpop('attackQueue', 30, (err, popped) => {
        if (err) {
            console.log(err);
        } else if (popped) {
            const currentDate = new Date();
            const attack = JSON.parse(popped[1]);

            let executable;
            switch (attack.language) {
                case 'python3':
                    executable = '/usr/bin/python3';
                    break;
                case 'python2':
                    executable = '/usr/bin/python2';
                    break;
            }

            let exploitPath = tempfile();
            fs.writeFileSync(exploitPath, attack.exploit);
            let historyId = uuid();

            // let collector create an empty attackResult document
            redis.lpush('attackResult', JSON.stringify({
                id: historyId,
                attackId: attack.id,
                attackTime: currentDate.getTime()
            }));
            let results = attack.targets.forEach((target) => {
                let stdout = '';
                let stderr = '';
                let timer = null;
                let startTime = new Date();

                let pushToCollector = (code) => {
                    let finishTime = new Date();
                    let flags = stdout.match(config.patternFlag);
                    if (flags && flags.length > 0) {
                        // 目前暫時只抓第一筆
                        let flag = flags[0];
                        submitFlag(flag, target.id, attack.chal_id).then((submitResult) => {
                          redis.lpush('attackResult', JSON.stringify({
                              id: historyId,
                              attackId: attack.id,
                              attackTime: currentDate.getTime(),
                              result: {
                                  startTime: startTime.getTime(),
                                  finishTime: finishTime.getTime(),
                                  chal_id: attack.chal_id,
                                  target: target.id,
                                  teamName: target.name,
                                  stdout: stdout,
                                  stderr: stderr,
                                  flag: flag || 'flag not found',
                                  submitResult: submitResult,
                                  code: code
                              }
                          }));
                        });
                    } else {
                      redis.lpush('attackResult', JSON.stringify({
                          id: historyId,
                          attackId: attack.id,
                          attackTime: currentDate.getTime(),
                          result: {
                              startTime: startTime.getTime(),
                              finishTime: finishTime.getTime(),
                              chal_id: attack.chal_id,
                              target: target.id,
                              teamName: target.name,
                              stdout: stdout,
                              stderr: stderr,
                              flag: 'flag not found',
                              submitResult: '',
                              code: code
                          }
                      }));
                    }
                };

                let process = child_process.spawn(executable, [exploitPath], {
                    env: {
                        TEAM_ID: target.id,
                        TEAM_NAME: target.name,
                        TARGET_HOST: target.host,
                        TARGET_PORT: target.port,
                        /* pwntool needs this env */
                        TERM: 'linux',
                        TERMINFO: '/etc/terminfo',
                        PATH: '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
                    }
                });
                process.stdout.on('data', (data) => {
                    stdout += data;
                });
                process.stderr.on('data', (data) => {
                    stderr += data;
                });
                process.on('exit', (code) => {
                    if (timer) clearTimeout(timer);
                    code === null ? pushToCollector('timeout') : pushToCollector(code);
                });
                timer = setTimeout(() => {
                    process.kill();
                }, config.workerTimeout);
            });
        }
        // next loop
        setTimeout(execute, 1);
    })
}

execute();
