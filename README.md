### Deploy

```
$ git clone https://gitlab.com/CykuTW/attackmanager.git
$ cd attackmanager
$ npm install
$ docker-compose build # 要裝 pwntool，有點ㄎㄅ久
$ docker-compose up --scale worker=10
```

**Clean**

```
$ docker-compose down --rmi 'local'
```

### Development

**執行方法**

在 config.js 設定好必要參數 (listen host, mongodb host, redis host)。

在三個不同 Terminal 中執行以下三個指令：
```
$ node app.js

$ node worker.js

$ node collector.js
```

**簡易架構圖**

![](static/architecture.jpg)

**TODO**

- [X] worker: 從 stdout 中 filter 出 flag
- [X] worker: 提交 flag
- [ ] app: 查看 attack history 的頁面
- [ ] app: 刪除 / 執行 / 暫停 攻擊排程
- [X] app: authentication 機制
- [ ] app: custom schedule
- [ ] app: 排程列表要輸出哪些部分詳細資訊
- [ ] app: parameters validation
- [ ] app: 執行結果以modal呈現
- [ ] app: 調整頁面底部margin
- [ ] app: 暫停功能
- [ ] app: 未登入時redirect
- [ ] app: 歷史紀錄搜尋功能
- [ ] app: i18n

**TODO (有生之年系列)**

- [ ] app: 攻擊成果的統計資料頁面
- [ ] mongodb: 優化儲存資料的方式
- [ ] app: refactor with some modern design pattern

### MongoDB Collection Schema

**attacks**

攻擊排程

```json
{
    "id": "4fc07bea-4c..",         // 排程 UUID，新增時自動產生
    "name": "[Web] BabyMeow",      // 攻擊排程名稱
    "exploit": "from pwn impo..",  // 攻擊腳本
    "schedule": "1m",              // 排程時間
    "language": "python2",         // 攻擊腳本使用的程式語言
    "challengeId": "4fc07bea-4c.." // 此排程要攻擊的目標 (對應題目的 targets 列表)
}
```

**challenges**

題目

```json
{
    "id": "4fc07bea-4c..",         // 題目 UUID，新增時自動產生
    "name": "[Web] BabyMeow",      // 題目名稱
    "targets": [
        {
            "id": "deadbeef",      // 目標隊伍 ID
            "name": "Meow",        // 目標隊伍名稱
            "host": "127.0.0.1",   // 目標隊伍題目主機的 IP 位址
            "port": 22             // 目標隊伍題目主機的連接埠
        }
    ]
}
```

**attackHistory**

記錄過往的攻擊結果

```json
{
    "id": "4fc07bea-4c..",                 // 記錄的 UUID，新增時自動產生
    "attackId": "4fc07bea-4c..",           // 此歷史記錄對應的攻擊排程 UUID
    "attackTime": 1575193409796,           // 攻擊排程執行的時間戳記
    "results": [
        {
            "startTime": 1575193409797,    // 腳本執行開始的時間戳記
            "finishTime":  1575193409798,  // 腳本執行結束的時間戳記
            "teamId": "deadbeef",          // 攻擊目標的隊伍 ID
            "teamName": "隊伍名稱",         // 攻擊目標的隊伍名稱
            "stdout": "aa",                // 攻擊腳本的 stdout
            "stderr": "aa",                // 攻擊腳本的 stderr
            "flag": "FLAG{a}"              // 攻擊完取得的 flag
        }
    ]
}
```