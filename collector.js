const config = require('./config');

const redis = require('redis').createClient(config.redis);
const MongoClient = require('mongodb').MongoClient;

redis.on('connect', function() {
    console.log('Redis client connected');
});

redis.on("error", function (err) {
    console.log("Error " + err);
});

MongoClient.connect(config.mongo.host, config.mongo.options, (err, connection) => {
    if (err) {
        console.error(`Cannot connect to database: ${config.mongo.host}`);
        process.exit();
    }

    const db = connection.db('attack-db');

    const collect = () => {
        redis.brpop('attackResult', 10, (err, popped) => {
            if (err) {
                console.log(err);
            } else if (popped) {
                const data = JSON.parse(popped[1]);
                const result = data.result || undefined;
                const filterForAttacks = { id: data.attackId };
                const filterForHistory = { id: data.id };

                if (result) {
                    // deep copy
                    // let lastAttack = JSON.parse(JSON.stringify(result));
                    // lastAttack.historyId = data.id;
                    db.collection('attacks').updateOne(filterForAttacks, {
                        $set: {
                            lastAttackHistoryId: data.id
                        }
                    });
                }

                // [issue]
                // $set cannot be empty
                // ref: https://felixc.at/2014/04/add-empty-set-support-back-for-pymongo-with-mongodb-2-6/
                let update = {};
                if (data.attackId || data.attackTime) {
                    update['$set'] = {
                        attackId: data.attackId,
                        attackTime: data.attackTime
                    };
                }
                if (result) {
                    update.$push = {
                        results: result
                    };
                }
                console.log(update);
                db.collection('attackHistory').updateOne(filterForHistory, update, { upsert: true, strict: false });
            }
            setTimeout(collect, 1);
        });
    };

    collect();
});